component table="nm_patient" extends="quick.models.BaseEntity" accessors="true"{

    property name = "id";
    property name = "fname";
    property name = "mname";
    property name = "lname";
    property name = "sex";
    property name = "dob";
    property name = "active";
    property name = "createdDate";
    property name = "updatedDate";

    function getMemento() {
        return {
            'id'           = getId(),
            'fname'        = getFName(),
            'mname'        = getMName(),
            'lname'        = getLName(),
            'sex'          = getSex(),
            'dob'          = dateFormat( getDob(), "MM/DD/YYYY" ),
            'active'       = getActive(),
            'createdDate'  = dateFormat( getCreatedDate(), "MM/DD/YYYY" ),
            'modifiedDate' = dateFormat( getAttribute( "modifiedDate" ), "MM/DD/YYYY" )
        };
    }
}
