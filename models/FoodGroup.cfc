component table="nm_foodGroup" extends="quick.models.BaseEntity"  accessors="true"{
    function foods() {
        return hasone( "food@nutrimps", "foodGroupId" );
    }
    property name = "id";
    property name = "name";
    property name = "createdDate";
    property name = "modifiedDate";
    
}
