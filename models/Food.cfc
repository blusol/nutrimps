component table="nm_food" extends="quick.models.BaseEntity" accessors="true" {
    function foodGroup() {
        return belongsto( "FoodGroup@nutrimps", "foodGroupID" );
    }
    property name = "id";
    property name = "foodGroupID";
    property name = "name";
    property name = "servings";
    property name = "servingSize";
    property name = "servingSizeUnit";
    property name = "calories";
    property name = "createdDate";
    property name = "modifiedDate";

    function getQuickMemento() {
        return {
            'id'              = getId(),
            'name'            = getName(),
            'servings'        = getServings(),
            'servingSize'     = getServingSize(),
            'servingSizeUnit' = getServingSizeUnit(),
            'calories'        = getCalories()
        };
    }
    function getMemento() {
        return {
            'id'              = getId(),
            'name'            = getName(),
            'servings'        = getServings(),
            'servingSize'     = getServingSize(),
            'servingSizeUnit' = getServingSizeUnit(),
            'calories'        = getCalories(),
            'foodgroup'       = this.getFoodGroup().getName(),
            'createdDate'     = dateFormat( getCreatedDate(), "MM/DD/YYYY" ),
            'modifiedDate'    = dateFormat( getAttribute( "modifiedDate" ), "MM/DD/YYYY" )
        };
    }
    
}
