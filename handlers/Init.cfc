component extends="nutrimps.handlers.BaseHandler"{
	property name="wirebox" inject="wirebox";
	
	function index( event, rc, prc ){
        var r = {}
		var foodGroups = getInstance( "foodGroup@nutrimps" )
							.all()
							.pluck("data")
                            .toArray();
        r['foodGroups'] = foodGroups
		prc.response.setData( r );
		
    }
    
    function get( event, rc, prc ){
		var foods = getInstance( "food@nutrimps" )
							.findOrFail( rc.id );
		prc.response.setData( foods.getMemento() );
		
	}

}
