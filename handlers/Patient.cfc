component extends="nutrimps.handlers.BaseHandler"{
	property name="wirebox" inject="wirebox";
	
	function index( event, rc, prc ){
		var patients = getInstance( "Patient@nutrimps" ).all().$renderData();
		prc.response.setData( patients );
		// writeDump(patients)
		// abort;
		
    }
    
    function get( event, rc, prc ){
		var patients = getInstance( "Patient@nutrimps" )
							.findOrFail( rc.id );
		prc.response.setData( patients.getMemento() );
		
	}

}
