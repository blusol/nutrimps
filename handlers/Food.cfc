component extends="nutrimps.handlers.BaseHandler"{
	property name = "wirebox" inject = "wirebox";
	property name = "fractal" inject = "Manager@cffractal";
	
	function index( event, rc, prc ){
		var foods = getInstance( "food@nutrimps" ).all().$renderData();
		var jFoods = fractal.collection(
			data = foods,
			transformer = function( food ) {
					return {
						'id'              = food.Id,
						'name'            = food.Name,
						'servings'        = food.Servings,
						'servingSize'     = food.ServingSize,
						'servingSizeUnit' = food.ServingSizeUnit,
						'calories'        = food.Calories,
						'createdDate'     = dateformat(food.createdDate, 'yyyy-mm-dd'),
						'modifiedDate'    = dateformat(food.modifiedDate, 'yyyy-mm-dd')
					};
			},
			serializer = wirebox.getInstance( "SimpleSerializer@cffractal" )
		);
		var scope = fractal.createData( jFoods );
		prc.response.setData( scope.convert() );
  }
    
	function get( event, rc, prc ){
		var foods = getInstance( "food@nutrimps" ).findOrFail( rc.id );
		prc.response.setData( foods.getMemento() );
		
	}

}
