/**
 * A ColdBox Event Handler
 */

//  extends="nutrimps.handlers.BaseHandler"

component extends="nutrimps.handlers.BaseHandler"{
	property name="wirebox" inject="wirebox";
	
	function index( event, rc, prc ){
		var patients = getInstance( "Patient@nutrimps" )
							.all()
							.pluck("data")
							.toArray();
		prc.response.setData( patients );
		
	}

}
