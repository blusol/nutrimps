component extends="nutrimps.handlers.BaseHandler"{
	property name = "wirebox" inject = "wirebox";
	property name = "fractal" inject = "Manager@cffractal";
	
	function index( event, rc, prc ){
		var foodGroups = getInstance( "foodGroup@nutrimps" ).all().$renderData();
		var jFoodGroups = fractal.collection(
			data = foodGroups,
			transformer = function( foodGroup ) {
					return {
						'id'           = foodGroup.id,
						'name'         = foodGroup.name,
						'createdDate'  = dateformat(foodGroup.createdDate, 'yyyy-mm-dd'),
						'modifiedDate' = dateformat(foodGroup.modifiedDate, 'yyyy-mm-dd')
					};
			},
			serializer = wirebox.getInstance( "SimpleSerializer@cffractal" )
		);
		var scope = fractal.createData( jFoodGroups );
		prc.response.setData( scope.convert() );
	}	
	
	function get( event, rc, prc ){
		var foodGroup = getInstance( "foodGroup@nutrimps" ).findOrFail( rc.id );
		var jFoodGroup = fractal.item(
			data = foodGroup,
			transformer = function( foodGroup ) {
					return {
						'id'           = foodGroup.getId(),
						'name'         = foodGroup.getName(),
						'createdDate'  = dateformat(foodGroup.getCreatedDate(), 'yyyy-mm-dd'),
						'modifiedDate' = dateformat(foodGroup.getModifiedDate(), 'yyyy-mm-dd')
					};
			},
			serializer = wirebox.getInstance( "SimpleSerializer@cffractal" )
		);
		var scope = fractal.createData( jFoodGroup );
		prc.response.setData( scope.convert() );
  }
	  


}
