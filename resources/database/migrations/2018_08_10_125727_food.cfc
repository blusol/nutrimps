component {
    
    function up( schema ) {
        schema.create( "nm_food", function( table ) {
            table.increments( "id" );
            table.unsignedInteger( "foodGroupId" ).references( "id" ).onTable( "nm_foodGroup" );
            table.string( "name" ).unique();
            table.integer( "servings" );
            table.integer( "servingSize" );
            table.string( "servingSizeUnit" );
            table.integer( "calories" );
            table.integer( "totalFat" ).nullable();
            table.string( "totalFatUnit" ).nullable();
            table.integer( "cholesterol" ).nullable();
            table.string( "cholesterolUnit" ).nullable();
            table.integer( "sodium" ).nullable();
            table.string( "sodiumUnit" ).nullable();
            table.integer( "totalCarbohydrate" ).nullable();
            table.string( "totalCarbohydrateUnit" ).nullable();
            table.integer( "protein" ).nullable();
            table.string( "proteinUnit" ).nullable();
            table.timestamp( "createdDate" );
            table.timestamp( "modifiedDate" );
        } );
    }

    function down( schema ) {
        
    }

}
