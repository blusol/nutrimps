component {
    
    function up( schema ) {
        schema.create( "nm_patient", function( table ) {
            table.increments( "id" );
            table.string( "fname" );
            table.string( "mname" ).nullable();
            table.string( "lname" );
            table.string( "sex" );
            table.date( "dob" );
            table.tinyInteger( "active" );
            table.timestamp( "createdDate" );
            table.timestamp( "updatedDate" );
        } );    
    }

    function down( schema ) {
        
    }

}
