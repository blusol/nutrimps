component {
    
    function up( schema ) {
        schema.create( "nm_patientIntake", function( table ) {
            table.increments( "id" );
            table.unsignedInteger( "patientID" ).references( "id" ).onTable( "nm_patient" );
            table.unsignedInteger( "foodID" ).references( "id" ).onTable( "nm_food" );
            table.unsignedInteger( "caregiverID" ).references( "id" ).onTable( "nm_caregiver" );
            table.decimal( "qty" );
            table.timestamp( "intakeDate" );
            table.timestamp( "createdDate" );
            table.timestamp( "modifiedDate" );
        } );
    }

    function down( schema ) {
        
    }

}
