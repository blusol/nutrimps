component {
    
    function up( schema ) {
        schema.create( "nm_patientVitalSign", function( table ) {
            table.increments( "id" );
            table.unsignedInteger( "patientID" ).references( "id" ).onTable( "nm_patient" );
            table.unsignedInteger( "caregiverID" ).references( "id" ).onTable( "nm_caregiver" );
            table.decimal( "weight" ).nullable();
            table.string( "weightUnit" ).nullable();
            table.integer( "bloodPressureSystolic" ).nullable();
            table.integer( "bloodPressureDiastolic" ).nullable();
            table.integer( "lungs" ).nullable();
            table.timestamp( "lastBM" ).nullable();
            table.integer( "siezures" ).nullable();
            table.timestamp( "takenDate" );
            table.timestamp( "createdDate" );
            table.timestamp( "modifiedDate" );
        } );
    }

    function down( schema ) {
        
    }

}
