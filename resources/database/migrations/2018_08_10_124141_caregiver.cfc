component {
    
    function up( schema ) {
        schema.create( "nm_caregiver", function( table ) {
            table.increments( "id" );
            table.string( "username" ).unique();
            table.string( "email" ).unique();
            table.string( "password" );
            table.string( "fname" );
            table.string( "mname" ).nullable();
            table.string( "lname" );
            table.date( "dob" ).nullable();
            table.tinyInteger( "active" );
            table.timestamp( "createdDate" );
            table.timestamp( "updatedDate" );
        } );    
    }

    function down( schema ) {
        
    }

}
