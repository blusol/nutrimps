component {
    
    function up( schema ) {
        schema.create( "nm_foodGroup", function( table ) {
            table.increments( "id" );
            table.string( "name" ).unique();
            table.timestamp( "createdDate" );
            table.timestamp( "modifiedDate" );
        } );
    }

    function down( schema ) {
        
    }

}
